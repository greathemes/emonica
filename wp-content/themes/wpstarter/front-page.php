<?php
/**
 * The front page template file.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

do_action( 'wpstarter_site_content_area_start' );

$parent_class = 'front-page-languages';

set_query_var( 'parent_class', $parent_class ); ?>

<section class='<?php echo esc_attr( $parent_class ); ?>'>

	<div class='<?php echo esc_attr( "{$parent_class}__container {$parent_class}__container--wrapper" ) ?>'>

		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Nauczamy również innych języków obcych.', 'TRANSLATE' ); ?></h2>
			<p class='<?php echo esc_attr( "{$parent_class}__subheading" ) ?>'><?php esc_html_e( 'Zdajemy sobie sprawę, że w dzisiejszym świecie znajomość wielu języków jest kluczowa, chętnie pomożemy!', 'TRANSLATE' ); ?></p>
		</header>

		<ul class='<?php echo esc_attr( "{$parent_class}__list {$parent_class}__list--unstyled" ) ?>'>

			<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imageContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'languages-english-icon' ); ?>
				</div>
				<div class='<?php echo esc_attr( "{$parent_class}__contentContainer" ) ?>'>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'>
						<?php printf(
							// translators: %s: HTML tag.
							esc_html__( '%sJęzyk%s Angielski', 'TRANSLATE' ), '<span>', '</span>'
						); ?>
					</h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Nullam eu lacus ut purus tincidunt ultricies a id neque. Pellentesque pellentesque metus vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.', 'TRANSLATE' ); ?></p>
				</div>
			</li>

			<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imageContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'languages-spanish-icon' ); ?>
				</div>
				<div class='<?php echo esc_attr( "{$parent_class}__contentContainer" ) ?>'>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'>
						<?php printf(
							// translators: %s: HTML tag.
							esc_html__( '%sJęzyk%s Hiszpański', 'TRANSLATE' ), '<span>', '</span>'
						); ?>
					</h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Nullam eu lacus ut purus tincidunt ultricies a id neque. Pellentesque pellentesque metus vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.', 'TRANSLATE' ); ?></p>
				</div>
			</li>

			<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imageContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'languages-german-icon' ); ?>
				</div>
				<div class='<?php echo esc_attr( "{$parent_class}__contentContainer" ) ?>'>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'>
						<?php printf(
							// translators: %s: HTML tag.
							esc_html__( '%sJęzyk%s Niemiecki', 'TRANSLATE' ), '<span>', '</span>'
						); ?>
					</h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Nullam eu lacus ut purus tincidunt ultricies a id neque. Pellentesque pellentesque metus vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.', 'TRANSLATE' ); ?></p>
				</div>
			</li>

			<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imageContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'languages-russian-icon' ); ?>
				</div>
				<div class='<?php echo esc_attr( "{$parent_class}__contentContainer" ) ?>'>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'>
						<?php printf(
							// translators: %s: HTML tag.
							esc_html__( '%sJęzyk%s Rosyjski', 'TRANSLATE' ), '<span>', '</span>'
						); ?>
					</h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Nullam eu lacus ut purus tincidunt ultricies a id neque. Pellentesque pellentesque metus vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.', 'TRANSLATE' ); ?></p>
				</div>
			</li>

		</ul>

	</div>

</section>

<?php $parent_class = 'front-page-about'; ?>

<section class='<?php echo esc_attr( $parent_class ); ?>'>

	<?php set_query_var( 'parent_class', $parent_class ); ?>
	<?php get_template_part( 'svg/front-page', 'about-top-blob' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__outerContainer" ) ?>'>

		<div class='<?php echo esc_attr( "{$parent_class}__container {$parent_class}__container--wrapper" ) ?>'>

			<?php get_template_part( 'svg/front-page', 'about-middle-blob' ); ?>

			<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
				<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Dlaczego my?', 'TRANSLATE' ); ?></h2>
				<p class='<?php echo esc_attr( "{$parent_class}__subheading" ) ?>'><?php esc_html_e( 'Etiam laoreet dolor eu augue luctus, non venenatis risus efficitur. Suspendisse blandit nibh magna, ac.', 'TRANSLATE' ); ?></p>
			</header>

			<ul class='<?php echo esc_attr( "{$parent_class}__list {$parent_class}__list--unstyled" ) ?>'>

				<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'about-group-icon' ); ?>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'><?php esc_html_e( 'Małe grupy', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Stawiamy na jakość! O wiele łatwiej jest nauczyć języka w małych grupkach, możemy wtedy poświęcić więcej czasu dla każdej osoby.', 'TRANSLATE' ); ?></p>
				</li>

				<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'about-fun-icon' ); ?>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'><?php esc_html_e( 'Nauka poprzez zabawę', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Zdajemy sobie sprawę, że nauka z maluchami jest nieco inna. Młodzież potrzebuje innej motywacji, czasami szybko się zniechęca i nudzi. Zachęcamy do nauki poprzez gry, piosenki, konkursy, bajki i wiele więcej :)', 'TRANSLATE' ); ?></p>
				</li>

				<li class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'about-communication-icon' ); ?>
					<h3 class='<?php echo esc_attr( "{$parent_class}__title" ) ?>'><?php esc_html_e( 'Swobodna komunikacja', 'TRANSLATE' ); ?></h3>
					<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Wiemy, że niektórych ludzi cechuje nieśmiałość oraz problemy z pokonaniem bariery przed mówieniem. Nie martw się, u nas kroczek po kroczku pokonamy Twoje trudności. Strach przed popełnianiem błędów? W końcu się uczysz, więc masz prawo je popełniać.', 'TRANSLATE' ); ?></p>
				</li>

			</ul>

			<a href='#' class='<?php echo esc_attr( "{$parent_class}__more {$parent_class}__more--button" ) ?>'><?php esc_html_e( 'Zobacz naszą ofertę', 'TRANSLATE' ); ?></a>

		</div>

	</div>

	<?php get_template_part( 'svg/front-page', 'about-bottom-blob' ); ?>

</section>

<?php $parent_class = 'front-page-testimonials'; ?>

<section class='<?php echo esc_attr( $parent_class ); ?>'>

	<div class='<?php echo esc_attr( "{$parent_class}__container {$parent_class}__container--wrapper" ) ?>'>

		<?php set_query_var( 'parent_class', $parent_class ); ?>
		<?php get_template_part( 'svg/front-page', 'testimonials-blob' ); ?>

		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Opinie naszych uczniów', 'TRANSLATE' ); ?></h2>
			<p class='<?php echo esc_attr( "{$parent_class}__subheading" ) ?>'><?php esc_html_e( 'Etiam laoreet dolor eu augue luctus, non venenatis risus efficitur. Suspendisse blandit nibh magna, ac.', 'TRANSLATE' ); ?></p>
		</header>

		<div class='<?php echo esc_attr( "{$parent_class}__itemsContainer owl-carousel" ) ?>'>

			<div class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imgContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'testimonials-name' ); ?>
				</div>
				<h3 class='<?php echo esc_attr( "{$parent_class}__name" ) ?>'><?php esc_html_e( 'Angelika Ostrowska', 'TRANSLATE' ); ?></h3>
				<p class='<?php echo esc_attr( "{$parent_class}__quote" ) ?>'><?php esc_html_e( 'Vestibulum faucibus congue malesuada. Quisque dictum libero eget velit hendrerit, vitae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu.', 'TRANSLATE' ); ?></p>
			</div>

			<div class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imgContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'testimonials-name' ); ?>
				</div>
				<h3 class='<?php echo esc_attr( "{$parent_class}__name" ) ?>'><?php esc_html_e( 'Angelika Ostrowska', 'TRANSLATE' ); ?></h3>
				<p class='<?php echo esc_attr( "{$parent_class}__quote" ) ?>'><?php esc_html_e( 'Vestibulum faucibus congue malesuada. Quisque dictum libero eget velit hendrerit, vitae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu.', 'TRANSLATE' ); ?></p>
			</div>

			<div class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imgContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'testimonials-name' ); ?>
				</div>
				<h3 class='<?php echo esc_attr( "{$parent_class}__name" ) ?>'><?php esc_html_e( 'Angelika Ostrowska', 'TRANSLATE' ); ?></h3>
				<p class='<?php echo esc_attr( "{$parent_class}__quote" ) ?>'><?php esc_html_e( 'Vestibtae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu.', 'TRANSLATE' ); ?></p>
			</div>

			<div class='<?php echo esc_attr( "{$parent_class}__item" ) ?>'>
				<div class='<?php echo esc_attr( "{$parent_class}__imgContainer" ) ?>'>
					<?php get_template_part( 'svg/front-page', 'testimonials-name' ); ?>
				</div>
				<h3 class='<?php echo esc_attr( "{$parent_class}__name" ) ?>'><?php esc_html_e( 'Angelika Ostrowska', 'TRANSLATE' ); ?></h3>
				<p class='<?php echo esc_attr( "{$parent_class}__quote" ) ?>'><?php esc_html_e( 'Vestibulum faucibus congue malesuada. Quisque dictum libero eget velit hendrerit, vitae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu.', 'TRANSLATE' ); ?></p>
			</div>

		</div>

	</div>

</section>

<?php do_action( 'wpstarter_site_content_area_end' );

get_footer();
