<?php $parent_class = get_query_var( 'parent_class' ); ?>

<svg class='<?php echo esc_attr( "{$parent_class}__bottomBlob" ) ?>' viewBox="0 0 1440 320" preserveAspectRatio="none">
	<path d="M0 224l40-5.3c40-5.7 120-15.7 200-5.4C320 224 400 256 480 240c80-16 160-80 240-96s160 16 240 5.3C1040 139 1120 85 1200 96s160 85 200 122.7l40 37.3v64H0z"/>
</svg>
