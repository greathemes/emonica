<?php $parent_class = get_query_var( 'parent_class' ); ?>

<svg class='<?php echo esc_attr( "{$parent_class}__topBlob" ) ?>' viewBox="0 0 1440 320" preserveAspectRatio="none">
	<path d="M0 64l60-5.3C120 53 240 43 360 69.3 480 96 600 160 720 186.7c120 26.3 240 16.3 360 0 120-15.7 240-37.7 300-48l60-10.7v192H0z"/>
</svg>
