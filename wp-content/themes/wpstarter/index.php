<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

do_action( 'wpstarter_site_content_area_start' );

if ( have_posts() ) :

	while ( have_posts() ) :

		the_post();

		the_content();

	endwhile;

endif;

do_action( 'wpstarter_site_content_area_end' );

get_footer();
