<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

get_header();

$parent_class = 'error-404'; ?>

<section class='<?php echo esc_attr( $parent_class ); ?>'>

	<span class='<?php echo esc_attr( "{$parent_class}__num-background" ); ?>'>404</span>

	<div class='<?php echo esc_attr( "{$parent_class}__container {$parent_class}__container--wrapper" ); ?>'>
		<span class='<?php echo esc_attr( "{$parent_class}__num" ); ?>'>404</span>
		<h1 class='<?php echo esc_attr( "{$parent_class}__heading" ); ?>'><?php esc_html_e( 'Ups! Strona nie została znaleziona!', 'TRANSLATE' ); ?></h1>
		<p class='<?php echo esc_attr( "{$parent_class}__content" ); ?>'>
			<?php printf(
				// translators: %1$s: Start of the HTML link tag.
				// translators: %2$s: End of the HTML link tag.
				esc_html__( 'Przepraszamy, wygląda na to, że to ślepa uliczka. Najlepiej będzie %1$swrócić do strony głównej%2$s.', 'TRANSLATE' ),
				'<a href="' . esc_url( home_url( '/' ) ) . '" class="' . "{$parent_class}__content-link" . '">', '</a>'
			); ?>
		</p>

	</div>

</section>

<?php get_footer();
