# Copyright (C) 2019 emonica
# This file is distributed under the same license as the emonica package.
msgid ""
msgstr ""
"Project-Id-Version: emonica\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: 404.php:20
msgid "Ups! Strona nie została znaleziona!"
msgstr ""

#. translators: %1$s: Start of the HTML link tag.
#: 404.php:25
msgid "Przepraszamy, wygląda na to, że to ślepa uliczka. Najlepiej będzie %1$swrócić do strony głównej%2$s."
msgstr ""

#: footer.php:26
msgid "Kontakt"
msgstr ""

#: footer.php:27
msgid "Skontaktuj się proszę z nami za pomocą formularza kontaktowego lub jeśli wolisz zadzwoń/napisz email, bardzo chętnie odpowiemy na wszystkie Twoje pytania!"
msgstr ""

#: footer.php:29
msgid "ul. Kalinkowa 80/2, 86-300 Grudziądz"
msgstr ""

#: footer.php:52
msgid "Zobacz nas na mapce"
msgstr ""

#: front-page.php:23
msgid "Nauczamy również innych języków obcych."
msgstr ""

#: front-page.php:24
msgid "Zdajemy sobie sprawę, że w dzisiejszym świecie znajomość wielu języków jest kluczowa, chętnie pomożemy!"
msgstr ""

#. translators: %s: HTML tag.
#: front-page.php:37
msgid "%sJęzyk%s Angielski"
msgstr ""

#: front-page.php:40, front-page.php:55, front-page.php:70, front-page.php:85
msgid "Nullam eu lacus ut purus tincidunt ultricies a id neque. Pellentesque pellentesque metus vitae libero. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia."
msgstr ""

#. translators: %s: HTML tag.
#: front-page.php:52
msgid "%sJęzyk%s Hiszpański"
msgstr ""

#. translators: %s: HTML tag.
#: front-page.php:67
msgid "%sJęzyk%s Niemiecki"
msgstr ""

#. translators: %s: HTML tag.
#: front-page.php:82
msgid "%sJęzyk%s Rosyjski"
msgstr ""

#: front-page.php:109
msgid "Dlaczego my?"
msgstr ""

#: front-page.php:110, front-page.php:156
msgid "Etiam laoreet dolor eu augue luctus, non venenatis risus efficitur. Suspendisse blandit nibh magna, ac."
msgstr ""

#: front-page.php:117
msgid "Małe grupy"
msgstr ""

#: front-page.php:118
msgid "Stawiamy na jakość! O wiele łatwiej jest nauczyć języka w małych grupkach, możemy wtedy poświęcić więcej czasu dla każdej osoby."
msgstr ""

#: front-page.php:123
msgid "Nauka poprzez zabawę"
msgstr ""

#: front-page.php:124
msgid "Zdajemy sobie sprawę, że nauka z maluchami jest nieco inna. Młodzież potrzebuje innej motywacji, czasami szybko się zniechęca i nudzi. Zachęcamy do nauki poprzez gry, piosenki, konkursy, bajki i wiele więcej :)"
msgstr ""

#: front-page.php:129
msgid "Swobodna komunikacja"
msgstr ""

#: front-page.php:130
msgid "Wiemy, że niektórych ludzi cechuje nieśmiałość oraz problemy z pokonaniem bariery przed mówieniem. Nie martw się, u nas kroczek po kroczku pokonamy Twoje trudności. Strach przed popełnianiem błędów? W końcu się uczysz, więc masz prawo je popełniać."
msgstr ""

#: front-page.php:135
msgid "Zobacz naszą ofertę"
msgstr ""

#: front-page.php:155
msgid "Opinie naszych uczniów"
msgstr ""

#: front-page.php:165, front-page.php:173, front-page.php:181, front-page.php:189
msgid "Angelika Ostrowska"
msgstr ""

#: front-page.php:166, front-page.php:174, front-page.php:190
msgid "Vestibulum faucibus congue malesuada. Quisque dictum libero eget velit hendrerit, vitae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu."
msgstr ""

#: front-page.php:182
msgid "Vestibtae blandit elit blandit. Donec placerat quam eget lacus porta volutpat. In pulvinar iaculis leo, a volutpat mauris imperdiet eu."
msgstr ""

#: header.php:25
msgid "Skip to content"
msgstr ""

#: header.php:38
msgid "Hide navigation"
msgstr ""

#: header.php:70
msgid "Show navigation"
msgstr ""

#. translators: %s: Span HTML tag.
#: header.php:83
msgid "%sSkuteczna nauka%s języka angielskiego w Grudziądzu!"
msgstr ""

#: header.php:85
msgid "Witaj! Jesteśmy pasjonatami swojej pracy i uwielbiamy przekazywać wiedzę w przystępny sposób. Serdecznie zapraszamy do kontaktu!"
msgstr ""

#: header.php:86
msgid "Zapisz się na zajęcia"
msgstr ""

#: offer.php:17
msgid "01. Oferta dla maluchów."
msgstr ""

#: offer.php:20
msgid "Kurs przeznaczony dla najmłodszych pociech. Dzieci w wieku przedszkolnym (od 4-5 lat) uczą się języka angielskiego poprzez zabawę. Zastosowanie urozmaiconych środków dydaktycznych - flashcards (karty edukacyjne), puzzles ( puzle ), tales (bajki w j. angielskim), matching exercises ( ćwiczenia ), quizes ( zgadywanki, rebusy), board games ( gry edukacyjne ), songs ( piosenki ), comics ( komiksy) oraz wiele innych urozmaici oraz wzbogaci naukę języka."
msgstr ""

#: offer.php:21
msgid "Dzieci przekonają się, że nauka to nie tylko nudne powtarzanie ale zabawa połączona z zapamiętywaniem. W naszej szkole nauczyciele doskonale odnajdują się w pracy z maluchami, poświęcają cała swoją uwagę dziecku. Bardzo często zajęciom towarzyszy śpiew i taniec co oprócz nauki języka zaspokaja ich potrzeby ruchowe.Co najważniejsze potrafią oni wzbudzić w maluchach ciekawość i zachęcić je do nauki."
msgstr ""

#: offer.php:22, offer.php:38, offer.php:57, offer.php:74, offer.php:92, offer.php:108
msgid "Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego."
msgstr ""

#: offer.php:33
msgid "02. Oferta dla dzieci"
msgstr ""

#: offer.php:36
msgid "Kurs przeznaczony dla dzieci w wieku szkolnym (6 -12 lat) Nauczyciel przygotowuje wiele różnorodnych pomocy naukowych. Dzieci nie tylko uczą się języka za pomocą podręcznika ale korzystają również z wielu interesujących środków dydaktycznych."
msgstr ""

#: offer.php:37
msgid "Czynnikiem, który wspomaga i urozmaica naukę jest zabawowy charakter spotkań. Dzieci aktywnie uczestniczą w zajęciach - śpiewają piosenki, odgrywają scenki."
msgstr ""

#: offer.php:51
msgid "Oferta dla nastolatków"
msgstr ""

#: offer.php:54
msgid "Kurs przeznaczony dla młodzieży w wieku gimnazjalnym. Kurs nastawiony jest na wzbogacenie słownictwa, wprowadzenie zaawansowanych struktur gramatyczno-leksykalnych jak również ciągłym ulepszaniu zdolności pisania, czytania i co najważniejsze mówienia w języku angielskim. "
msgstr ""

#: offer.php:55
msgid "Podczas kursu przerabiane są niezliczone ilości tekstów, ćwiczeń gramatycznych oraz zadań na słownictwo. Dodatkowo nauczyciel wprowadza pierwsze rozmowy sterowane, które pomagają w rozwijaniu zdolności mówienia. Hipotetyczne zdarzenia i scenki pomagają w nakreśleniu sytuacji, w której mógłby się znaleźć kursant poza granicami kraju."
msgstr ""

#: offer.php:56
msgid "Na życzenie kursantów kurs zamieniamy w kurs przygotowujący do egzaminu gimnazjalnego lub inny dopasowany do ucznia. W takim przypadku oprócz korzystania z podręcznika omawiane są również arkusze egzaminacyjne oraz większy nacisk nastawiony jest na zadania egzaminacyjne. Godzina lekcyjna trwa 55 minut a terminarz kursów dopasowany jest do kalendarza szkolnego."
msgstr ""

#: offer.php:68
msgid "Oferta dla młodzieży"
msgstr ""

#: offer.php:71
msgid "Kurs przeznaczony dla kursantów przygotowujących się do różnego rodzaju egzaminów. Głównie przygotowujemy do egzaminu maturalnego. Pracujemy na specjalistycznych podręcznikach, arkuszach egzaminacyjnych oraz prowadzimy rozmowy sterowane."
msgstr ""

#: offer.php:72
msgid "Dodatkowo analizujemy arkusze egzaminacyjne z lat ubiegłych zarówno na poziomie podstawowym i rozszerzonym. Po ukończeniu kursu kursant uwrażliwiony jest na typowe błędy popełniane przez maturzystów i co więcej wie jak się ich wystrzegać."
msgstr ""

#: offer.php:73
msgid "Mnogość ćwiczeń i konwersacji sprawia że osoba przystępująca do egzaminu jest świadoma i odpowiednio przygotowana do niego. Polecamy wszystkim uczniom szkół średnich. Godzina lekcyjna trwa 55 minut a terminarz kursów dopasowany jest do kalendarza szkolnego."
msgstr ""

#: offer.php:87
msgid "Oferta dla dorosłych"
msgstr ""

#: offer.php:90
msgid "Kurs przeznaczony dla osób dorosłych pragnących poszerzać umiejętności językowe. Skierowany jest dla osób na wszystkich poziomach językowych. Kurs przeznaczony jest dla osób lubiących podróże i osób które na co dzień w pracy posługują się językiem angielskim. Intensywny kurs pozwoli kursantom powtórzyć zrealizowany materiał oraz przyswoić nowe słownictwo, rozszerzyć gramatykę oraz usprawnić umiejętność mówienia w języku angielskim."
msgstr ""

#: offer.php:91
msgid "Mogą z niego skorzystać również osoby, które będą poszukiwały pracy zagranicą. Podczas zajęć kursant nauczy się jak sprawnie posługiwać się językiem angielskim zarówno na krótkoterminowych wyjazdach turystycznych oraz podczas dłuższego pobytu zarobkowego. Ponadto pozna przydatne zwroty, słownictwo oraz podstawy gramatyki angielskiej niezbędnej w porozumiewaniu się z obcokrajowcami. Godzina lekcyjna trwa 55 minut a terminarz zajęć dopasowany jest do godzin pracy kursanta."
msgstr ""

#: offer.php:103
msgid "Oferta dla seniorów"
msgstr ""

#: offer.php:106
msgid "Kurs przeznaczony dla osób, które uważają, że na naukę nigdy nie jest za późno. Język angielski nie tylko będzie pomocny podczas wyjazdów turystycznych poza granice Polski ale również przy pomocy wnukom."
msgstr ""

#: offer.php:107
msgid "Zajęcia nie tylko pomogą usprawnić zdolności językowe ale rozwinąć towarzysko i intelektualnie. Intensywność zajęć oraz wymiar godzin będzie zależał od indywidualnych potrzeb oraz aspiracji kursantów. Godzina lekcyjna trwa 55 minut a terminarz spotkań dopasowany jest do możliwości czasowych kursantów."
msgstr ""

#: inc/class-wpstarter-walker-primary.php:114
msgid "Pokaż menu rozwijane"
msgstr ""

#: inc/setup.php:74
msgid "Primary Navigation"
msgstr ""

#: inc/setup.php:75
msgid "Footer Navigation"
msgstr ""

#: inc/setup.php:199
msgid "Primary Sidebar"
msgstr ""

#: inc/setup.php:201
msgid "Tu dodaj swoje widżety."
msgstr ""

#: inc/template-functions.php:60
msgid "Zapraszamy do zapoznania się z naszymi propozycjami. Dla każdego coś miłego!"
msgstr ""
