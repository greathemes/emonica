<?php
/**
 * The template for displaying the footer.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */ ?>

	</div>

</div>

<?php if ( ! is_404() ) : ?>

	<footer class='footer'>

		<?php get_template_part( 'svg/footer', 'blob' ); ?>

		<div class='footer__outerWrapper'>
			<div class='footer__wrapper footer__wrapper--wrapper'>

				<div class='footer__contactWrapper' id='footer__contactWrapper'>
					
					<div class='footer__contact'>
						<h3 class='footer__contactHeading'><?php esc_html_e( 'Kontakt', 'TRANSLATE' ) ?></h3>
						<p class='footer__contactDescription'><?php esc_html_e( 'Skontaktuj się proszę z nami za pomocą formularza kontaktowego lub jeśli wolisz zadzwoń/napisz email, bardzo chętnie odpowiemy na wszystkie Twoje pytania!', 'TRANSLATE' ) ?></p>
						<div class='footer__contactDetails'>
							<span class='footer__contactDetail'><?php esc_html_e( 'ul. Kalinkowa 80/2, 86-300 Grudziądz', 'TRANSLATE' ) ?></span>
							<a class='footer__contactDetail' href='tel:662-548-753'>Tel: 662-548-753</a>
							<a class='footer__contactDetail' href='mailto:monicaszkola@wp.pl'>Email: monicaszkola@wp.pl</a>
						</div>
					</div>

					<div class='footer__form'><?php echo do_shortcode( '[contact-form-7 id="27" title="Contact"]', false ); ?></div>

				</div>

				<div class='footer__nav'>
					<?php if ( has_nav_menu( 'footer' ) ) :
						wp_nav_menu( [
							'theme_location' => 'footer',
							'container'      => false,
							'items_wrap'     => '<ul class="footer__navList footer__navList--unstyled">%3$s</ul>',
							'walker'         => new Wpstarter_Walker_Primary( 'footer', true ),
						] );
					endif; ?>
				</div>

				<?php get_template_part( 'svg/footer', 'people' ); ?>

				<span class='footer__copyright'>&copy; 2013-<?php echo date( 'Y' ); ?> emonica <a href='https://www.google.com/maps/place/Emonica/@53.4744735,18.7396503,15z/data=!4m2!3m1!1s0x0:0xdd05e17c541ce78b?sa=X&ved=2ahUKEwia7YSBkdPlAhU86KYKHaYmBSwQ_BIwCnoECA4QCA' target='_blank'><?php esc_html_e( 'Zobacz nas na mapce', 'TRANSLATE' ) ?></a></span>

			</div>
		</div>

		<a href='#primary-header' class='footer__backToTop footer__backToTop--button footer__backToTop--is-hidden fa fa-long-arrow-alt-up'></a>

	</footer>

<?php endif; ?>

<?php wp_footer(); ?>
