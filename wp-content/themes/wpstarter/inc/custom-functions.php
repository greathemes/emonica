<?php
/**
 * Helper functions & filters.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_additional_body_classes' ) ) :

	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 * @return array
	 */
	function wpstarter_additional_body_classes( $classes ) {

		// Front page.
		$classes[] = is_front_page() ? 'front-page' : '';

		// Non-singular pages.
		$classes[] = ( ! is_singular() ) ? 'hfeed' : '';

		return $classes;

	}

endif;

add_filter( 'body_class', 'wpstarter_additional_body_classes' );

// Remove 'p' tags from Contact Form 7 output.
add_filter( 'wpcf7_autop_or_not', '__return_false' );

/**
 * Deregister Contact Form 7 scripts and styles.
 */
add_action( 'wp_enqueue_scripts', function() {

	if ( ! is_admin() ) :
		wp_dequeue_script( 'contact-form-7' );
		wp_dequeue_style( 'contact-form-7' );
	endif;

} );

if ( ! function_exists( 'wpstarter_remove_block_library_css' ) ) :

	/**
	 * Deregister block-library.css.
	 */
	function wpstarter_remove_block_library_css() {

		wp_dequeue_style( 'wp-block-library' );

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_remove_block_library_css' );

if ( ! function_exists( 'wpstarter_remove_jquery_migrate' ) ) :

	/**
	 * Remove JQuery migrate.
	 */
	function wpstarter_remove_jquery_migrate( $scripts ) {

		if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) :

				$script = $scripts->registered['jquery'];

				// Check whether the script has any dependencies.
				if ( $script->deps ) :
					$script->deps = array_diff( $script->deps, [
						'jquery-migrate'
					] );
				endif;

		endif;

	}

endif;

add_action( 'wp_default_scripts', 'wpstarter_remove_jquery_migrate' );

if ( ! function_exists( 'wpstarter_disable_emojis' ) ) :

	/**
	 * Disable the emoji's
	 */
	function wpstarter_disable_emojis() {

		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );	
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'tiny_mce_plugins', 'wpstarter_disable_emojis_tinymce' );
		add_filter( 'wp_resource_hints', 'wpstarter_disable_emojis_remove_dns_prefetch', 10, 2 );

	}

endif;

add_action( 'init', 'wpstarter_disable_emojis' );

if ( ! function_exists( 'wpstarter_disable_emojis_tinymce' ) ) :

	/**
	 * Filter function used to remove the tinymce emoji plugin.
	 * 
	 * @param    array  $plugins  
	 * @return   array             Difference betwen the two arrays
	 */
	function wpstarter_disable_emojis_tinymce( $plugins ) {

		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		}

		return array();

	}

endif;

if ( ! function_exists( 'wpstarter_disable_emojis_remove_dns_prefetch' ) ) :

	/**
	 * Remove emoji CDN hostname from DNS prefetching hints.
	 *
	 * @param  array  $urls          URLs to print for resource hints.
	 * @param  string $relation_type The relation type the URLs are printed for.
	 * @return array                 Difference betwen the two arrays.
	 */
	function wpstarter_disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {

		if ( 'dns-prefetch' == $relation_type ) {

			// Strip out any URLs referencing the WordPress.org emoji location
			$emoji_svg_url_bit = 'https://s.w.org/images/core/emoji/';
			foreach ( $urls as $key => $url ) {
				if ( strpos( $url, $emoji_svg_url_bit ) !== false ) {
					unset( $urls[$key] );
				}
			}

		}

		return $urls;

	}

endif;
