<?php
/**
 * Nav Menu API: Walker_Nav_Menu class
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

if ( ! class_exists( 'Wpstarter_Walker_Primary' ) ) :

	/**
	 * Wpstarter_Walker_Primary.
	 *
	 * The class used to remodel the primary navigation extended by the core class used to implement an HTML list of nav menu items.
	 */
	class Wpstarter_Walker_Primary extends Walker_Nav_menu {

		function __construct( $css_class_prefix ) {

				$this->css_class_prefix        = $css_class_prefix;
				$this->item_css_class_suffixes = [
					'item'                    => '__navItem',  
					'parent_item'             => '__navItem--parent',
					'active_item'             => '__navItem--active',
					'parent_of_active_item'   => '__navItem--parent--active',
					'ancestor_of_active_item' => '__navItem--ancestor--active',
					'sub_menu'                => '__navSubMenu',
					'sub_menu_item'           => '__navSubMenuItem',
					'sub_menu_btn'            => '__navBtn',
					'sub_menu_btn_icon'       => '__navBtnIcon',
					'link'                    => '__navLink',
				];

		}

		function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
								
				$id_field = $this->db_fields['id'];
				
				if ( is_object( $args[0] ) ) :
					$args[0]->has_children = ! empty( $children_elements[ $element->$id_field ] );
				endif;
				
				return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
		
		}

		function start_lvl( &$output, $depth = 1, $args = [] ) {
				
				$real_depth = $depth + 1;
				
				$indent = str_repeat( "\t", $real_depth );
				$prefix = $this->css_class_prefix;
				$suffix = $this->item_css_class_suffixes;
				$classes = [
					$prefix . $suffix['sub_menu'],
					$prefix . $suffix['sub_menu']. '--' . $real_depth
				];
				$class_names = implode( ' ', $classes );
				$output .= "\n" . $indent . '<ul class="'. $class_names .'">' ."\n";
		}

		function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
				
				global $wp_query;
				
				$indent = ( $depth > 0 ? str_repeat( "    ", $depth ) : '' );
				$prefix = $this->css_class_prefix;
				$suffix = $this->item_css_class_suffixes;
				// Item classes
				$item_classes = [
					'item_class'            => $depth === 0 ? $prefix . $suffix['item'] : '',
					'parent_class'          => $args->has_children ? $parent_class = $prefix . $suffix['parent_item'] : '',
					'active_page_class'     => in_array( 'current-menu-item', $item->classes) ? $prefix . $suffix['active_item'] : '',
					'active_parent_class'   => in_array( 'current-menu-parent', $item->classes) ? $prefix . $suffix['parent_of_active_item'] : '',
					'active_ancestor_class' => in_array( 'current-menu-ancestor', $item->classes) ? $prefix . $suffix['ancestor_of_active_item'] : '',
					'depth_class'           => $depth >= 1 ? $prefix . $suffix['sub_menu_item'] . ' ' . $prefix . $suffix['sub_menu'] . '--' . $depth . '__navItem' : '',
					'item_id_class'         => $prefix . '__navItem--' . $item->object_id,
					'user_class'            => $item->classes[0] !== '' ? $prefix . '__navItem--' . $item->classes[0] : ''
				];

				$class_string = implode( '  ', array_filter( $item_classes ) );
				$output .= $indent . '<li class="' . $class_string . '">';
				$link_classes = [
					'item_link'   => $depth === 0 ? $prefix . $suffix['link'] : '',
					'depth_class' => $depth >= 1 ? $prefix . $suffix['sub_menu'] . $suffix['link'] . '  ' . $prefix . $suffix['sub_menu'] . '--' . $depth . $suffix['link'] : '',
				];
				$link_class_string = implode( '  ', array_filter( $link_classes ) );
				$link_class_output = 'class="' . $link_class_string . '"';

				// Link attributes.
				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) . '"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target ) . '"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn ) . '"' : '';
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url ) . '"' : '';

				// Create link markup.
				$item_output = $args->before;
				$item_output .= '<a' . $attributes . ' ' . $link_class_output . '>';
				$item_output .= $args->link_before;
				$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
				$item_output .= $args->link_after;
				$item_output .= $args->after;
				$item_output .= '</a>';

				// Create sub menu button markup.
				if ( $args->has_children ) :

					$class_string = implode( '  ', array_filter( $item_classes ) );

					$item_output .= '<button type="button" class="' . $prefix . ( $depth >= 1 ? $suffix['sub_menu'] : '' ) . $suffix['sub_menu_btn'] . '">';
					$item_output .= '<span class="' . $prefix . ( $depth >= 1 ? $suffix['sub_menu'] : '' ) . $suffix['sub_menu_btn_icon'] . ' fas ' . ( $depth >= 1 ? 'fa-angle-right' : 'fa-angle-down' ) . '" aria-hidden="true"></span>';
					$item_output .= '<span class="screen-reader-text">' . esc_html__( 'Pokaż menu rozwijane', 'TRANSLATE' ) . '</span>';
					$item_output .= '</button>';
					
				endif;

				// Filter <li>
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}

	}

endif;

