<?php
/**
 * Custom template & tag functions.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

	if ( ! function_exists( 'wpstarter_header_primary_navigation' ) ) :

		/**
		 * Displays the primary navigation.
		 */
		function wpstarter_header_primary_navigation( array $args = [] ) {

			if ( has_nav_menu( 'primary' ) ) :

				$parent_class = isset( $args['parent_class'] ) ? $args['parent_class'] : ''; ?>

				<nav class='<?php echo esc_attr( "$parent_class" ) ?>'>

					<div class='<?php echo esc_attr( "{$parent_class}__container" ) ?>'>

						<?php wp_nav_menu( [
							'theme_location' => 'primary',
							'container'      => false,
							'items_wrap'     => "<ul class='{$parent_class}__list {$parent_class}__list--unstyled'>" . '%3$s' . "</ul>",
							'walker'         => new Wpstarter_Walker_Primary( $parent_class, true ),
						] ); ?>

					</div>

				</nav>

			<?php endif; ?>

		<?php }

	endif;

if ( ! function_exists( 'wpstarter_site_content_header_area' ) ) :

	function wpstarter_site_content_header_area() {

		if ( ! is_front_page() ) :

			$parent_class = 'page-header' ?>

			<div class='<?php echo esc_attr( $parent_class ); ?>'>

					<header class='<?php echo esc_attr( "{$parent_class}__header" ); ?>'>

						<h1 class='<?php echo esc_attr( "{$parent_class}__heading" ); ?>'>
							<?php echo wp_kses_post( get_the_title( get_queried_object_id() ) ); ?>
						</h1>

						<?php if ( is_page( 11 ) ) : ?>
							<p class='<?php echo esc_attr( "{$parent_class}__subheading" ); ?>'>
								<?php esc_html_e( 'Zapraszamy do zapoznania się z naszymi propozycjami. Dla każdego coś miłego!', 'TRANSLATE' ); ?>
							</p>
						<?php endif; ?>

					</header>

			</div>

		<?php endif;

	}

endif;

if ( ! function_exists( 'wpstarter_site_content_area_start' ) ) :

	function wpstarter_site_content_area_start() { ?>

		<div class='content-area content-area--wrapper'>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_start' ) ) :

	function wpstarter_site_content_primary_area_start( $page_layout = false ) { ?>

		<div class='primary-area'>

			<main id='main' class='site-main'>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_primary_area_end' ) ) :

	function wpstarter_site_content_primary_area_end( $page_layout = false ) { ?>

			</main>

		</div>

	<?php }

endif;

if ( ! function_exists( 'wpstarter_site_content_area_end' ) ) :

	function wpstarter_site_content_area_end() { ?>

		</div>

	<?php }

endif;
