<?php
/**
 * Custom hooks for wpstarter theme.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

// Site content area start.
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_area_start', 10 );
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_header_area', 20 );
add_action( 'wpstarter_site_content_area_start', 'wpstarter_site_content_primary_area_start', 30, 1 );

// Site content area end.
add_action( 'wpstarter_site_content_area_end', 'wpstarter_site_content_primary_area_end', 10, 1 );
add_action( 'wpstarter_site_content_area_end', 'wpstarter_site_content_area_end', 20, 0 );
