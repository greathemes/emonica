<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @since 1.0.0
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package wpstarter
 */

if ( ! function_exists( 'wpstarter_setup' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpstarter_setup() {

		// Make theme available for translation.
		load_theme_textdomain( 'TRANSLATE', WPSTARTER_THEME_DIR . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 *
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'editor-styles' );

		add_theme_support( 'responsive-embeds' );

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', [ 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ] );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			[
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			]
		);

		/**
		 * Register all menus.
		 */
		register_nav_menus(
			[
				'primary' => esc_html__( 'Primary Navigation', 'TRANSLATE' ),
				'footer'  => esc_html__( 'Footer Navigation', 'TRANSLATE' ),
			]
		);

	}

endif;

add_action( 'after_setup_theme', 'wpstarter_setup' );

if ( ! function_exists( 'wpstarter_modify_max_srcset_image_width' ) ) :

	/**
	 * Modifies the maximum width of the srcset attribute.
	 *
	 * @param int $max_width Width of the image.
	 */
	function wpstarter_modify_max_srcset_image_width( $max_width ) {

		$max_width = 2200;

		return $max_width;

	}

endif;

add_filter( 'max_srcset_image_width', 'wpstarter_modify_max_srcset_image_width' );

if ( ! function_exists( 'wpstarter_enqueue_styles' ) ) :

	/**
	 * Enqueue styles.
	 */
	function wpstarter_enqueue_styles() {

		wp_enqueue_style( 'wpstarter-style', get_stylesheet_uri(), [], '1.0.0', 'all' );
		// wp_enqueue_style( 'wpstarter-styles-min', get_stylesheet_directory_uri() . '/assets/css/styles.min.css', [], '1.0.0', 'all' );

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_enqueue_styles' );

if ( ! function_exists( 'wpstarter_enqueue_assets' ) ) :

	/**
	 * Enqueue assets.
	 */
	function wpstarter_enqueue_assets() {

		// JS.
		wp_enqueue_script( 'wpstarter-scripts-min', WPSTARTER_THEME_URI . 'scripts.min.js', [ 'jquery' ], '1.0.0', true );

		// Original snippet from Contact Form 7 plugin (It's all because of combine plugin script with the theme's script to make one request less).
		if ( class_exists( 'WPCF7' ) ) :

			$wpcf7 = array(
				'apiSettings' => array(
					'root' => esc_url_raw( rest_url( 'contact-form-7/v1' ) ),
					'namespace' => 'contact-form-7/v1',
				),
			);

			if ( defined( 'WP_CACHE' ) and WP_CACHE ) {
				$wpcf7['cached'] = 1;
			}

			wp_localize_script( 'wpstarter-scripts-min', 'wpcf7', $wpcf7 );

		endif;

		// Loads extra JS.
		if ( is_singular( 'post' ) && comments_open() && get_option( 'thread_comments' ) ) :
			wp_enqueue_script( 'comment-reply' );
		endif;

		// Google fonts.
		if ( ! is_admin() ) :

			// Setup font arguments.
			$args = [ 'family' => 'Work+Sans:400,500,600,700,800' ];

			// A safe way to register a CSS style file for later use.
			wp_register_style( 'wpstarter-google-fonts', add_query_arg( $args, '//fonts.googleapis.com/css' ), [], '1.0.0', 'all' );

			// A safe way to add/enqueue a CSS style file to a WordPress generated page.
			wp_enqueue_style( 'wpstarter-google-fonts' );

		endif;

	}

endif;

add_action( 'wp_enqueue_scripts', 'wpstarter_enqueue_assets' );

if ( ! function_exists( 'wpstarter_enqueue_admin_assets' ) ) :

	/**
	 * Enqueue admin assets.
	 */
	function wpstarter_enqueue_admin_assets( $hook ) {

		// Media.
		! did_action( 'wp_enqueue_media' ) ? wp_enqueue_media() : '';

	}

endif;

add_action( 'admin_enqueue_scripts', 'wpstarter_enqueue_admin_assets' );

if ( ! function_exists( 'wpstarter_register_widgets' ) ) :

	/**
	 * Register all widgets.
	 */
	function wpstarter_register_widgets() {

		// Primary Sidebar.
		register_sidebar(
			[
				'name'          => esc_html__( 'Primary Sidebar', 'TRANSLATE' ),
				'id'            => 'primary',
				'description'   => esc_html__( 'Tu dodaj swoje widżety.', 'TRANSLATE' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h3 class="widget__title"><span>',
				'after_title'   => '</span></h3>',
			]
		);

	}

endif;

add_action( 'widgets_init', 'wpstarter_register_widgets' );

if ( ! function_exists( 'wpstarter_content_width' ) ) :

	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function wpstarter_content_width() {

		$GLOBALS['content_width'] = apply_filters( 'wpstarter_content_width', 1110 );

	}

endif;

add_action( 'template_redirect', 'wpstarter_content_width', 0 );

if ( ! function_exists( 'wpstarter_pingback_header' ) ) :

	/**
	 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
	 */
	function wpstarter_pingback_header() {

		if ( is_singular() && pings_open() ) :

			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';

		endif;

	}

endif;

add_action( 'wp_head', 'wpstarter_pingback_header' );
