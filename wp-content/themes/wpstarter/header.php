<?php
/**
 * The header for wpstarter theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id='content'>.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset='<?php bloginfo( 'charset' ); ?>'>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<link rel='profile' href='//gmpg.org/xfn/11'>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class='site-container'>

		<a class='skip-link screen-reader-text' href='#site-area'><?php esc_html_e( 'Skip to content', 'TRANSLATE' ); ?></a>

		<?php if ( ! is_404() ) : ?>

			<?php $parent_class = 'primary-header'; ?>
			<?php set_query_var( 'parent_class', $parent_class ); ?>

			<header class='<?php echo esc_attr( $parent_class . ( is_front_page() ? " {$parent_class}--front-page" : '' ) ); ?>' id='primary-header' data-nav='500'>

				<div class='<?php echo esc_attr( "{$parent_class}__mobile" ); ?>'>
					<div class='<?php echo esc_attr( "{$parent_class}__mobileContainer" ); ?>'>
						<button type='button' class='<?php echo esc_attr( "{$parent_class}__mobileHideBtn" ); ?>'>
							<span class='<?php echo esc_attr( "{$parent_class}__mobileHideBtnIcon fas fa-times" ); ?>' aria-hidden='true'></span>
							<span class='screen-reader-text'><?php esc_html_e( 'Hide navigation', 'TRANSLATE' ); ?></span>
						</button>
						<?php wpstarter_header_primary_navigation( [
							'parent_class' => "{$parent_class}__mobileNav"
						] ); ?>
					</div>
				</div>

				<div class='<?php echo esc_attr( "{$parent_class}__desktop" ); ?>'>
					
					<div class='<?php echo esc_attr( "{$parent_class}__desktopContainer {$parent_class}__desktopContainer--wrapper" ) ?>'>

						<?php $logo = get_bloginfo( 'name' );

						if ( $logo ) : ?>

							<div class='<?php echo esc_attr( "{$parent_class}__identity" ); ?>'>
								<a class='<?php echo esc_attr( "{$parent_class}__identityLink" ); ?>' href='<?php echo esc_url( home_url( '/' ) ); ?>' rel='home'>

									<?php if ( is_front_page() ) : ?>
										<h1 class='<?php echo esc_attr( "{$parent_class}__identityLogo" ); ?>'><?php echo wp_kses_post( $logo ); ?></h1>
									<?php else : ?>
										<p class='<?php echo esc_attr( "{$parent_class}__identityLogo" ); ?>'><?php echo wp_kses_post( $logo ); ?></p>
									<?php endif; ?>

								</a>
							</div>

						<?php endif; ?>

						<button type='button' class='<?php echo esc_attr( "{$parent_class}__desktopShowBtn" ) ?>'>
							<span class='<?php echo esc_attr( "{$parent_class}__desktopShowBtnIcon fas fa-bars" ) ?>' aria-hidden='true'></span>
							<span class='screen-reader-text'><?php esc_html_e( 'Show navigation', 'TRANSLATE' ); ?></span>
						</button>

						<?php wpstarter_header_primary_navigation( [
							'parent_class' => "{$parent_class}__desktopNav"
						] ); ?>

						<?php if ( is_front_page() ) : ?>

							<div class='<?php echo esc_attr( "{$parent_class}__welcome" ); ?>'>
								<div class='<?php echo esc_attr( "{$parent_class}__welcomeContentContainer" ); ?>'>
									<h1 class='<?php echo esc_attr( "{$parent_class}__welcomeHeading" ); ?>'><?php printf(
										// translators: %s: Span HTML tag.
										esc_html__( '%sSkuteczna nauka%s języka angielskiego w Grudziądzu!', 'TRANSLATE' ), '<span>', '</span>'
									); ?></h1>
									<p class='<?php echo esc_attr( "{$parent_class}__welcomeDescription" ); ?>'><?php esc_html_e( 'Witaj! Jesteśmy pasjonatami swojej pracy i uwielbiamy przekazywać wiedzę w przystępny sposób. Serdecznie zapraszamy do kontaktu!', 'TRANSLATE' ) ?></p>
									<a href='<?php echo esc_url( get_permalink( 14 ) ); ?>' class='<?php echo esc_attr( "{$parent_class}__welcomeLink {$parent_class}__welcomeLink--button" ); ?>'><?php esc_html_e( 'Zapisz się na zajęcia', 'TRANSLATE' ) ?></a>
								</div>
								<div class='<?php echo esc_attr( "{$parent_class}__welcomeImgContainer" ); ?>'>
									<?php get_template_part( 'svg/primary-header', 'people' ); ?>
								</div>
							</div>

						<?php endif; ?>

					</div>

				</div>

				<?php get_template_part( 'svg/primary-header', 'bottom-blob' ); ?>

			</header>

			<div id='site-area' class='site-area'>

		<?php endif; ?>
