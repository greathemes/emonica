<?php
/**
 * Functions and definitions.
 *
 * @since 1.0.0
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package-wpstarter
 */

// Constants.
define( 'WPSTARTER_THEME_DIR', trailingslashit( get_template_directory() ) );
define( 'WPSTARTER_THEME_INCLUDES', trailingslashit( get_template_directory() . '/inc' ) );
define( 'WPSTARTER_THEME_URI', trailingslashit( esc_url( get_template_directory_uri() ) ) );

// Setup.
require_once WPSTARTER_THEME_INCLUDES . 'setup.php';

// Helper Functions.
require_once WPSTARTER_THEME_INCLUDES . 'custom-functions.php';

// Walker Classes.
require_once WPSTARTER_THEME_INCLUDES . 'class-wpstarter-walker-primary.php';

// Template hooks & functions.
require_once WPSTARTER_THEME_INCLUDES . 'template-hooks.php';
require_once WPSTARTER_THEME_INCLUDES . 'template-functions.php';
