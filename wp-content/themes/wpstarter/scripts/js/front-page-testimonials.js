/**
 * Front Page testimonials slider.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

jQuery( function( $ ) {

	'use strict';

	const $owl = $( '.front-page-testimonials__itemsContainer' );

	if ( $owl.length ) {

		$owl.owlCarousel( {
			items             : 1,
			center            : false,
			nav               : false,
			dots              : true,
			loop              : false,
			checkVisible      : false,
			autoHeight        : true,
			rewind            : true,
			autoplay          : false,
			autoplayTimeout   : false,
			autoplayHoverPause: false,
		} );

	}

} );
