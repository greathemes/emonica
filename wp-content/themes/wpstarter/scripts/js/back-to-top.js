/**
 * Back to top button.
 *
 * @since 1.0.0
 *
 * @package wpstarter
 */

( function() {

	'use strict';

	const toTop = document.querySelector( '.footer__backToTop' );

	if ( toTop ) {

		window.addEventListener('scroll', function() {

			if ( document.body.scrollTop >= 1000 || window.pageYOffset >= 1000 ) {
				toTop.classList.remove( 'footer__backToTop--is-hidden' );
				toTop.classList.add( 'footer__backToTop--is-visible' );
			} else {
				toTop.classList.add( 'footer__backToTop--is-hidden' );
				toTop.classList.remove( 'footer__backToTop--is-visible' );
			}

		}, false);

	}

}() );
