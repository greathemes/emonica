<?php /* Template Name: Oferta */

get_header();

do_action( 'wpstarter_site_content_area_start' );

$parent_class = 'offer';

set_query_var( 'parent_class', $parent_class ); ?>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--titch" ) ?>'>

	<?php get_template_part( 'svg/offer', 'titch-icon' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--titch" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( '01. Oferta dla maluchów.', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla najmłodszych pociech. Dzieci w wieku przedszkolnym (od 4-5 lat) uczą się języka angielskiego poprzez zabawę. Zastosowanie urozmaiconych środków dydaktycznych - flashcards (karty edukacyjne), puzzles ( puzle ), tales (bajki w j. angielskim), matching exercises ( ćwiczenia ), quizes ( zgadywanki, rebusy), board games ( gry edukacyjne ), songs ( piosenki ), comics ( komiksy) oraz wiele innych urozmaici oraz wzbogaci naukę języka.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Dzieci przekonają się, że nauka to nie tylko nudne powtarzanie ale zabawa połączona z zapamiętywaniem. W naszej szkole nauczyciele doskonale odnajdują się w pracy z maluchami, poświęcają cała swoją uwagę dziecku. Bardzo często zajęciom towarzyszy śpiew i taniec co oprócz nauki języka zaspokaja ich potrzeby ruchowe.Co najważniejsze potrafią oni wzbudzić w maluchach ciekawość i zachęcić je do nauki.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

</section>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--kid" ) ?>'>

	<?php get_template_part( 'svg/offer', 'top-blob' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--kid" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( '02. Oferta dla dzieci', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla dzieci w wieku szkolnym (6 -12 lat) Nauczyciel przygotowuje wiele różnorodnych pomocy naukowych. Dzieci nie tylko uczą się języka za pomocą podręcznika ale korzystają również z wielu interesujących środków dydaktycznych.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Czynnikiem, który wspomaga i urozmaica naukę jest zabawowy charakter spotkań. Dzieci aktywnie uczestniczą w zajęciach - śpiewają piosenki, odgrywają scenki.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

	<?php get_template_part( 'svg/offer', 'kid-icon' ); ?>

</section>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--teenager" ) ?>'>

	<?php get_template_part( 'svg/offer', 'teenager-icon' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--teenager" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Oferta dla nastolatków', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla młodzieży w wieku gimnazjalnym. Kurs nastawiony jest na wzbogacenie słownictwa, wprowadzenie zaawansowanych struktur gramatyczno-leksykalnych jak również ciągłym ulepszaniu zdolności pisania, czytania i co najważniejsze mówienia w języku angielskim. ', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Podczas kursu przerabiane są niezliczone ilości tekstów, ćwiczeń gramatycznych oraz zadań na słownictwo. Dodatkowo nauczyciel wprowadza pierwsze rozmowy sterowane, które pomagają w rozwijaniu zdolności mówienia. Hipotetyczne zdarzenia i scenki pomagają w nakreśleniu sytuacji, w której mógłby się znaleźć kursant poza granicami kraju.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Na życzenie kursantów kurs zamieniamy w kurs przygotowujący do egzaminu gimnazjalnego lub inny dopasowany do ucznia. W takim przypadku oprócz korzystania z podręcznika omawiane są również arkusze egzaminacyjne oraz większy nacisk nastawiony jest na zadania egzaminacyjne. Godzina lekcyjna trwa 55 minut a terminarz kursów dopasowany jest do kalendarza szkolnego.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

</section>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--young" ) ?>'>

	<?php get_template_part( 'svg/offer', 'middle-blob' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--young" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Oferta dla młodzieży', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla kursantów przygotowujących się do różnego rodzaju egzaminów. Głównie przygotowujemy do egzaminu maturalnego. Pracujemy na specjalistycznych podręcznikach, arkuszach egzaminacyjnych oraz prowadzimy rozmowy sterowane.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Dodatkowo analizujemy arkusze egzaminacyjne z lat ubiegłych zarówno na poziomie podstawowym i rozszerzonym. Po ukończeniu kursu kursant uwrażliwiony jest na typowe błędy popełniane przez maturzystów i co więcej wie jak się ich wystrzegać.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Mnogość ćwiczeń i konwersacji sprawia że osoba przystępująca do egzaminu jest świadoma i odpowiednio przygotowana do niego. Polecamy wszystkim uczniom szkół średnich. Godzina lekcyjna trwa 55 minut a terminarz kursów dopasowany jest do kalendarza szkolnego.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

	<?php get_template_part( 'svg/offer', 'young-icon' ); ?>

</section>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--adult" ) ?>'>

	<?php get_template_part( 'svg/offer', 'adult-icon' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--adult" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Oferta dla dorosłych', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla osób dorosłych pragnących poszerzać umiejętności językowe. Skierowany jest dla osób na wszystkich poziomach językowych. Kurs przeznaczony jest dla osób lubiących podróże i osób które na co dzień w pracy posługują się językiem angielskim. Intensywny kurs pozwoli kursantom powtórzyć zrealizowany materiał oraz przyswoić nowe słownictwo, rozszerzyć gramatykę oraz usprawnić umiejętność mówienia w języku angielskim.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Mogą z niego skorzystać również osoby, które będą poszukiwały pracy zagranicą. Podczas zajęć kursant nauczy się jak sprawnie posługiwać się językiem angielskim zarówno na krótkoterminowych wyjazdach turystycznych oraz podczas dłuższego pobytu zarobkowego. Ponadto pozna przydatne zwroty, słownictwo oraz podstawy gramatyki angielskiej niezbędnej w porozumiewaniu się z obcokrajowcami. Godzina lekcyjna trwa 55 minut a terminarz zajęć dopasowany jest do godzin pracy kursanta.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

</section>

<section class='<?php echo esc_attr( "{$parent_class} {$parent_class}--old" ) ?>'>

	<?php get_template_part( 'svg/offer', 'bottom-blob' ); ?>

	<div class='<?php echo esc_attr( "{$parent_class}__contentContainer {$parent_class}__contentContainer--old" ) ?>'>
		<header class='<?php echo esc_attr( "{$parent_class}__header" ) ?>'>
			<h2 class='<?php echo esc_attr( "{$parent_class}__heading" ) ?>'><?php esc_html_e( 'Oferta dla seniorów', 'TRANSLATE' ); ?></h2>
		</header>

		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Kurs przeznaczony dla osób, które uważają, że na naukę nigdy nie jest za późno. Język angielski nie tylko będzie pomocny podczas wyjazdów turystycznych poza granice Polski ale również przy pomocy wnukom.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__description" ) ?>'><?php esc_html_e( 'Zajęcia nie tylko pomogą usprawnić zdolności językowe ale rozwinąć towarzysko i intelektualnie. Intensywność zajęć oraz wymiar godzin będzie zależał od indywidualnych potrzeb oraz aspiracji kursantów. Godzina lekcyjna trwa 55 minut a terminarz spotkań dopasowany jest do możliwości czasowych kursantów.', 'TRANSLATE' ); ?></p>
		<p class='<?php echo esc_attr( "{$parent_class}__details {$parent_class}__details--titch" ) ?>'><?php esc_html_e( 'Godzina lekcyjna z maluchami trwa 30-45 minut. Cena uzależniona jest od liczby małych kursantów a terminarz kursów dopasowany jest do kalendarza przedszkolnego.', 'TRANSLATE' ); ?></p>
	</div>

	<?php get_template_part( 'svg/offer', 'old-icon' ); ?>

</section>

<?php do_action( 'wpstarter_site_content_area_end' );

get_footer();
